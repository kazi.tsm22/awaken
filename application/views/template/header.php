  <?php
    if (!empty($this->session->userdata('user_id'))) {
        $is_login = 1;
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
    } else {
        $is_user_login = 0;
    }

    if ($this->session->flashdata('flash_data')) {
        $flash_message = $this->session->flashdata('flash_data');
    }
  ?>

  <?php
  $ci =& get_instance();
  if ($this->session->userdata('user_id')>0) {
      //$get_wishlist_data = $ci->User_model->get_data_from_wishlist_by_user_id($this->session->userdata('user_id'));
      $get_cart_data = $ci->User_model->get_data_from_cart_by_user_id($this->session->userdata('user_id'));
      $extra_data = array('get_cart_data' =>$get_cart_data);
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/bootstrap-5.0.2-dist/css/bootstrap.min.css');?>" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/youtube-video-popup/youtube-overlay.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>" />
    <title>Actuation</title>
  </head>
  <body class="<?php echo $this->uri->segment(2)?>">
    <!-- Site Header -->
    <header class="site-header">
      <div class="container">
        <div class="row align-items-center justify-content-between py-3">
          <a href="<?php echo base_url();?>" class="d-inline-block site-header__logo">
            <img src="<?php echo base_url('assets/images/logo.png');?>" alt="logo" />
          </a>
          <nav class="site-header__nav d-none d-lg-inline-block w-auto">
            <ul class="d-flex justify-content-center align-items-center">
              <li>
                <a href="<?php echo base_url();?>">Home</a>
              </li>
              <li>
                <a target="_blank" href="https://actuationtestequipment.com/Index_files/AboutATECo.html">About Us</a>
              </li>
              <li>
                <a href="<?php echo base_url('home/products');?>">Products</a>
              </li>
              <li>
                <a href="<?php echo base_url('home/contact-us');?>">Contact Us</a>
              </li>
            </ul>
          </nav>
          <div class="d-flex align-items-center w-auto">

            <div class="dropdown">
              <a href="" class="site-header__button--my-account d-inline-block me-2 dropdown-toggle" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi bi-person-fill"></i></a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <?php if ($this->session->userdata('user_id')):?>
                  <li><a class="dropdown-item" href="<?php echo base_url('home/user_dashboard');?>"><i class="bi bi-person"> </i> My account</a></li>
                  <li><a class="dropdown-item" href="<?php echo base_url('home/logout');?>"><i class="bi bi-box-arrow-left"></i> Logout</a></li>
                <?php else:?>
                  <li><a class="dropdown-item" href="<?=base_url('home/login_page');?>"><i class="bi bi-person"> </i>Login</a></li>
                <?php endif;?>
              </ul>
            </div>

            <?php if ($this->session->userdata('user_id')):?>
              <a href="<?php echo base_url('user/cart');?>" class="site-header__button--cart d-inline-block ms-2"><i class="bi bi-bag"></i><span class="cart-item-count"><?php echo count($extra_data['get_cart_data'])?></span></a>
            <?php else:?>
              <a class="site-header__button--cart d-inline-block ms-2"><i class="bi bi-bag"></i></a>
            <?php endif;?>
            <button class="site-header__button--nav-toggler d-inline-block d-lg-none ms-2" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavigation" aria-controls="offcanvasNavigation"><i class="bi bi-list"></i></button>
          </div>
        </div>
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavigation" aria-labelledby="offcanvasNavigationLabel">
          <div class="offcanvas-header justify-content-end">
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          </div>
          <div class="offcanvas-body">
            <nav class="site-header__mobile-nav">
              <ul>
                <li>
                <a href="<?php echo base_url();?>">Home</a>
              </li>
              <li>
                <a target="_blank" href="https://actuationtestequipment.com/Index_files/AboutATECo.html">About Us</a>
              </li>
              <li>
                <a href="<?php echo base_url('home/products');?>">Products</a>
              </li>
              <li>
                <a href="<?php echo base_url('home/contact_us');?>">Contact Us</a>
              </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>

    <style>
      .dropdown-item.active, .dropdown-item:active {
          color: #000 !important;
          text-decoration: none;
          background-color: #f7f7f7 !important;
      }
    </style>


    <script>
      <?php
        $config = array(
          'base_url'=>base_url('/').''
        );
        echo 'var global_config='. json_encode($config).';';
      ?>
    </script>
