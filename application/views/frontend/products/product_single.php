<?php $this->load->view('template/header');
    $images = json_decode($data['product'][0]['img'], true);
    //print_r(count($images));
?>

<main class="site-content">
      <section class="page-title-section d-flex justify-content-center align-items-center">
        <div class="container">
          <h3 class="page-title text-center"><?php echo $data['product'][0]['name']?></h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb d-flex justify-content-center p-0">
              <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $data['product'][0]['name']?></li>
            </ol>
          </nav>
        </div>
      </section>

      <!-- product-single section -->
      <div class="product-single-section section--padding">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-lg-10 col-xl-11 col-xxl-10">
              <div class="row">
                <div class="col-12 col-md-6 pb-3 pb-md-0">
                  <div class="prouduct-single__thumbnail-image-gallery row flex-column-reverse flex-xl-row">
                    <div class="col-12 col-md-12 col-xl-2 mt-2 mt-xl-0">
                      <div thumbsSlider="" class="swiper product-single-swiper-thumb">
                        <div class="swiper-wrapper d-xl-flex flex-xl-column">
                            <?php for ($i =0;$i<count($images);$i++):?>
                                <div class="swiper-slide">
                                    <img src="<?php print_r(base_url('assets/images/products/').$images['images_'.$i])?>" />
                                </div>
                            <?php endfor;?>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-md-12 col-xl-10">
                      <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper product-single-swiper-view">
                        <div class="swiper-wrapper">
                            <?php for ($i =0;$i<count($images);$i++):?>
                                <div class="swiper-slide" style="height:500px">
                                    <img class="img-responsive" src="<?php print_r(base_url('assets/images/products/').$images['images_'.$i])?>" style="height:500px">
                                </div>
                            <?php endfor;?>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6 pt-4 pt-md-0 ps-md-4 ps-xl-5">
                  <h3 class="product-single__title text--heading"><?php echo $data['product'][0]['name']?></h3>
                  <p class="text--para product-single__short-desc text--sm"><?php echo substr($data['product'][0]['description'], 0, 220)?>......</p>
                  <div class="product-single__price-container d-flex align-items-center mb-3">
                    <h3 class="product-single__sell-price text--primary">$<?php echo $data['product'][0]['selling_price']?></h3>
                    <h5 class="product-single__regular-price text--para">$<?php echo $data['product'][0]['mrp']?></h5>
                  </div>
                  <div class="product-single__quantity-button-wrapper d-flex align-items-center mb-4">
                    <div class="form__field d-flex align-items-center">
                      <label for="quantity" class="text--primary me-3">Quantity</label>
                      <div class="product-single__quantity-button quantity-generating-input-container">
                        <input type="button" value="-" class="qtyminus" field="quantity" />
                        <input type="text" id="add_to_cart_qty" name="quantity" value="1" data-qty="1" class="qty" />
                        <input type="button" value="+" class="qtyplus" field="quantity" />
                      </div>
                    </div>
                  </div>

                  <div class="product-single__cta-wrapper mb-4 d-flex">  
                    <?php if ($this->session->userdata('user_id')):?>
                      <a  href="javascript:void(0)" id="add_to_cart" data_product_id ="<?php echo $data['product'][0]['id']?>" data_userid ="<?php echo $this->session->userdata('user_id');?>"  class="button button-primary d-inline-block product-single__cta--add-to-cart">Add to Cart</a>
                    <?php else:?>
                     <a onclick="notificationToastUpdate('error', 'You have to login to add this product into the Cart List')" class="button button__primary d-inline-block product-single__cta--add-to-cart">Add to Cart</a>
                    <?php endif;?>
                  </div>
                  <div class="product-single__meta-info d-flex align-items-center">
                    <p class="text--para me-2 mb-0">
                        Share:
                    </p>
                    <ul class="product-single__social-sharing d-flex">
                      <li class="social-sharing__item">
                        <a href="#" class="social-sharing__item--link fb--link" target="_blank">
                          <span class="social-sharing__item-icon"><i class="bi bi-facebook"></i></span>
                          <span class="social-sharing__item-tooltip tooltip-text">Facebook</span>
                        </a>
                      </li>
                      <li class="social-sharing__item">
                        <a href="#" class="social-sharing__item--link insta--link" target="_blank">
                          <span class="social-sharing__item-icon"><i class="bi bi-instagram"></i></span>
                          <span class="social-sharing__item-tooltip tooltip-text">Instagram</span>
                        </a>
                      </li>
                      <li class="social-sharing__item">
                        <a href="#" class="social-sharing__item--link linkedin--link" target="_blank">
                          <span class="social-sharing__item-icon"><i class="bi bi-linkedin"></i></span>
                          <span class="social-sharing__item-tooltip tooltip-text">Linkedin</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-12 mt-5">
                  <h4 class="text--heading product-single__inner-section-heading">Description</h4>
                  <div class="product-single__description mb-5">
                    <p class="text--para"><?php echo $data['product'][0]['description']?></p>
                    <!-- <ul>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                    </ul> -->
                    <!-- <p class="text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, et.</p> -->
                  </div>
                  <!-- <h4 class="text--heading product-single__inner-section-heading">Additional information</h4>
                  <div class="product-single__addi-info">
                    <ul class="product-single__addi-info-list">
                      <li>
                        <span class="addi-info__title me-2 text--heading">Type Of Packing:</span>
                        <span class="addi-info__text text--primary">Bottle</span>
                      </li>
                      <li>
                        <span class="addi-info__title me-2 text--heading">Color:</span>
                        <span class="addi-info__text text--primary">Green, Pink, Powder Blue, Purple:</span>
                      </li>
                      <li>
                        <span class="addi-info__title me-2 text--heading">Quantity Per Case:</span>
                        <span class="addi-info__text text--primary">100ml</span>
                      </li>
                      <li>
                        <span class="addi-info__title me-2 text--heading">Ethyl Alcohol:</span>
                        <span class="addi-info__text text--primary">70%</span>
                      </li>
                      <li>
                        <span class="addi-info__title me-2 text--heading">Ethyl Alcohol:</span>
                        <span class="addi-info__text text--primary">Carton</span>
                      </li>
                    </ul>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>

<?php $this->load->view('template/footer');?>

<script>
  $(document).ready(function(){
    $('#add_to_cart').click(function(){
      var product_id = $(this).attr("data_product_id");
      var user_id = $(this).attr("data_userid");
      var qty = $('input#add_to_cart_qty').attr("data-qty");
      var url = "user/update_add_to_cart";
      
      $.ajax({
        url: global_config.base_url+url,
        type: 'POST',
        data: {product_id: product_id, user_id: user_id, qty: qty},
        error: function() {
          notificationToastUpdate('error', "The product Has been added to Wishlist successfully!");
        },
        success: function(data) {
          notificationToastUpdate('success', "The product Has been added to Wishlist successfully!");
          location.reload();
        }
      });
    });
  });

  
  function notificationToastUpdate(msgtype, msg) {
    var toastEl = $("#liveToast");
    toastEl.find(".toast-body__text").html(msg);
    if (msgtype === "success") {
      toastEl.find(".toast-body").addClass("text--secondary");
      toastEl.find(".toast-body__icon").html(`<i class="bi bi-check-circle-fill"></i>`);
    } else if (msgtype === "error" || msgtype === "server_error") {
      toastEl.find(".toast-body").addClass("text--error");
      toastEl.find(".toast-body__icon").html(`<i class="bi bi-x-circle-fill"></i>`);
    }else {
      toastEl.find(".toast-body").addClass("text--para");
    }
    toastEl.toast("show");
  }

  </script>