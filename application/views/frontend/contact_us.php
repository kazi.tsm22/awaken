<?php $this->load->view('template/header')?>;

    <main class="site-content">
      <section class="page-title-section d-flex justify-content-center align-items-center">
        <div class="container">
          <h3 class="page-title text-center">Contact Us</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb d-flex justify-content-center p-0">
              <li class="breadcrumb-item"><a href="./">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
            </ol>
          </nav>
        </div>
      </section>
      <section>
        <div class="contact-section-top bg--primary section--padding-top-only pb-5">
          <div class="container">
            <div class="row justify-content-center text-center text-lg-start">
              <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="contact-info">
                  <div class="contact-info__icon">
                    <i class="bi bi-telephone-fill"></i>
                  </div>
                  <div class="contact-info__content">
                    <p class="text--heading">Call us</p>
                    <p class="d-flex mb-3 justify-content-center justify-content-lg-start"><span>ph:</span><a href="tel:(815) 335-1143" class="d-inline-block ms-2">(815) 335-1143</a></p>
                    <p class="d-flex mb-0 justify-content-center justify-content-lg-start"><span>Fx:</span><a href="tel:(302) 336-1143" class="d-inline-block ms-2">(302) 336-1143</a></p>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-4 mt-4 mb-4 mt-md-0 mb-lg-0">
                <div class="contact-info">
                  <div class="contact-info__icon">
                    <i class="bi bi-envelope-fill"></i>
                  </div>
                  <div class="contact-info__content">
                    <p class="text--heading">Send us an email</p>
                    <a href="mailto:DudleyDevices@Aol.com" class="d-block">DudleyDevices@Aol.com</a>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="contact-info">
                  <div class="contact-info__icon">
                    <i class="bi bi-geo-alt-fill"></i>
                  </div>
                  <div class="contact-info__content">
                    <p class="text--heading">Locate Us</p>
                    <a href="#" class="d-block" target="_blank">3393 Eddie Road Winnebago, Illinois 61088</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="contact-section-bottom section--padding-bottom-only bg--light pt-5 position-relative">
          <div class="container position-relative">
            <div class="row justify-content-center mx-1 mx-md-0">
              <div class="col-12 col-lg-12 col-xl-11">
                <div class="row contact-form-wrapper border-radius--custom overflow-hidden box-shadow--custom">
                  <div class="col-12 col-lg-6 p-0">
                    <div class="contact-map-container h-100">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d33381.09904401537!2d-89.20908049032934!3d42.31052918685435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880892252937aca7%3A0x2770075f11c0e86d!2s3393%20Eddie%20Rd%2C%20Winnebago%2C%20IL%2061088%2C%20USA!5e0!3m2!1sen!2sin!4v1651489679180!5m2!1sen!2sin" width="100%" height="100%" style="border: 0" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6 p-0">
                    <div class="contact-form-container px-3 py-4 px-sm-4 p-md-5 bg--white">
                      <h3 class="text--heading mt-2 mt-md-0">We'd love to hear you</h3>
                      <p class="text--sm text--para mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, eligendi.</p>
                      <form action="#" class="form contact-form mb-2 mb-md-0 mt-md-3" id="contactForm">
                        <div class="form__field">
                          <input type="text" name="c_fullname" placeholder="Full Name*" />
                        </div>
                        <div class="form__field">
                          <input type="email" name="c_email" placeholder="Email*" />
                        </div>
                        <div class="form__field">
                          <input type="text" name="c_phone" placeholder="Phone*" />
                        </div>
                        <div class="form__field">
                          <textarea name="c_message" placeholder="Message"></textarea>
                        </div>
                        <div class="d-flex justify-content-end">
                          <button type="submit" class="button button-primary d-lg-block w-100 text-center">Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    

<?php $this->load->view('template/footer')?>;
