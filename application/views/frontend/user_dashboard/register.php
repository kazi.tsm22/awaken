<?php $this->load->view('template/header')?>

<main class="site-content">
      <section class="page-title-section d-flex justify-content-center align-items-center">
        <div class="container">
          <h3 class="page-title text-center">Register</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb d-flex justify-content-center p-0">
              <li class="breadcrumb-item"><a href="./">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Register</li>
            </ol>
          </nav>
        </div>
      </section>
      <section class="user-action-section user-action-section--register section--padding position-relative bg--light">
        <div class="container">
            <?php if (form_error('name') || form_error('email_addr') || form_error('mobile_number')) { ?>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 p-0">
                            <div class="alert alert-danger">
                                <?php echo form_error('name'); ?>
                                <?php echo form_error('email_addr'); ?>
                                <?php echo form_error('mobile_number'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
          <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-lg-12 col-xl-11 col-xxl-10">
              <div class="form-container row box-shadow--custom border-radius--custom mx-1 mx-md-0">
                <div class="col-12 col-md-12 col-lg-7 form-container__content-col px-3 py-4 p-md-4 p-lg-5 bg--white">
                    <h4 class="text--heading font--serif mb-3">Sign Up</h4>
                    <?php echo form_open_multipart('home/user_register', array('class'=>'form register-form', 'autocomplete'=> 'off'))?>
                        <div class="row">
                            <div class="form__field col-12 col-md-6">
                                <label for="username" class="d-block mb-2 text--heading">Full Name*</label>
                                <?php echo form_input(['name'=>'name', 'class'=> 'bg--light', 'value'=> set_value('name')])?>
                            </div>
                            <div class="form__field col-12 col-md-6">
                                <label for="username" class="d-block mb-2 text--heading">Email*</label>
                                <?php echo form_input(['name'=>'email_addr', 'class'=> 'bg--light', 'value'=> set_value('email_addr')])?>
                            </div>
                        </div>

                        <div class="form__field">
                            <label for="username" class="d-block mb-2 text--heading">Phone*</label>
                            <?php echo form_input(['name'=>'mobile_number', 'class'=> 'bg--light', 'value'=> set_value('mobile_number')])?>
                        </div>

                        <div class="form__field mb-4">
                            <label for="username" class="d-block mb-2 text--heading">Password*</label>
                            <?php echo form_input(['name'=>'password', 'class'=> 'bg--light', 'value'=> set_value('password')])?>
                            <button class="button--show-password p-1"><i class="bi bi-eye"></i></button>
                        </div>

                        <button type="submit" class="button button-primary d-block w-100">Sign Up</button>
                        <p class="text--para mt-3 mb-0 text-center">Already have an account? <a href="./login.html" class="redirecting-link--login fw-semibold text-decoration-underline">Login</a></p>
                    <?php echo form_close();?>
                </div>
                <div class="col-12 col-md-12 col-lg-5 form-container__img-col"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    

<?php $this->load->view('template/footer')?>