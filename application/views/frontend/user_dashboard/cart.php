<?php $this->load->view('template/header')?>

<main class="site-content">
  <section class="page-title-section d-flex justify-content-center align-items-center">
    <div class="container">
      <h3 class="page-title text-center">Login</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb d-flex justify-content-center p-0">
          <li class="breadcrumb-item"><a href="./">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Cart</li>
        </ol>
      </nav>
    </div>
  </section>

  <section class="section--padding">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-12 col-lg-8 col-xxl-9" id="cart_table">
          <div class="table-responsive">
            <table class="table cart-data-table">
              <thead>
                <tr class="bg--primary text--light">
                  <th scope="col">Product</th>
                  <th scope="col">Price</th>
                  <th scope="col">Quantity</th>
                  <th scope="col">Subtotal</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                  <?php $product_id = 0;
                        $totalamount = 0;
                          foreach ($data['cart_product'] as $cart_product) :?>
                        <tr class="cart-data-table__row">
                            <td>
                                <a href="<?php echo base_url('home/product_details/'.$cart_product['product_id']);?>" class="d-flex align-items-center cart-data-table__product">
                                <span class="cart-data-table__product-image me-3">
                                    <img src="<?php echo base_url('assets/images/products/'.json_decode($cart_product['img'], true)['images_0']);?>" alt="product1" />
                                </span>
                                <span class="cart-data-table__product-title"><?php echo $cart_product['name']?></span>
                                </a>
                            </td>
                            <td> <b> $<?php echo $cart_product['selling_price'].'.00';?></b></td>
                            <td>
                                <div class="quantity-generating-input-container">
                                    <input readonly name="quantity1" value="<?php echo $cart_product['qty']?>" class="qty" />
                                </div>
                            </td>
                            <td><b> $<?php echo  $subtotal = $cart_product['selling_price'] * $cart_product['qty'].'.00';?></b></td>
                            <td>
                                <a href="javascript:void(0)" onclick="deletecartproduct(<?php echo $cart_product['id']?>)" class="cart-data-table__button--remove-row"><i class="bi bi-x-lg"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <?php
                                $totalamount = $totalamount+$subtotal;
                                $product_details[] = array('product_id'=>$cart_product['product_id'], 'product_qty'=>$cart_product['qty'], 'selling_price'=>$cart_product['selling_price'], 'subtotal' => $cart_product['selling_price']*$cart_product['qty']);
                            ?>
                        </tr>
                    <?php endforeach;?>
              </tbody>
            </table>
          </div>
          <p class="empty-cart-text text-center d-none error-text mt-3">No item has been added to the cart.</p>
          <div class="d-flex justify-content-between align-items-center mt-4">
            <a href="<?php echo base_url('home/products')?>" class="button button-secondary">Continue Shopping</a>
            <a href="javascript:void(0)" onclick="deleteAllCartProduct(<?php echo $this->session->userdata('user_id')?>)" class="cart-data-table__button--clear-total-items ms-3 text-uppercase">
              <span><i class="bi bi-x-lg"></i></span>
              <span>Clear</span>
            </a>
          </div>
        </div>

        <section class="section--padding col-12 col-md-12 col-lg-8 col-xxl-9 pt-0" id="address_section" style="display:none;margin-top: -3%;">
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12 mb-5 mb-lg-0 pe-lg-5">
                <div class="checkout-steps mx-4 mx-sm-0">
                  <div class="checkout-step__content mt-4">
                    <div class="address-container">
                      <ul class="address-saved-list row">
                        <?php foreach ($data['user_address'] as $user_address): ?>
                          <li class="address-saved-list__item col-12 col-sm-6 col-xl-6 selected" <?php if ($user_address['is_default'] > 0) {
                                echo 'data_address='.$user_address['id'].' id="address_id"';
                            }?> style="margin: 10px 0;">
                            <div class="p-4 <?php if ($user_address['is_default'] > 0) {
                                echo 'bg--light';
                            } else {
                                echo " ";
                            }?> border-radius--custom border-custom-1">
                              <p class="text--xs text--primary m-0 p-2"><?php echo $user_address['address']." , ".$user_address['city']." , ". $user_address['zip_code']." , Near: ".$user_address['landmark']?></p>
                              <a href="javascript:void(0)" class="address-saved-list__item--select-btn" style="font-size: 18px;"><i class="bi <?php if ($user_address['is_default'] > 0) {
                                echo 'bi-check-circle-fill';
                            } else {
                                echo "bi-check-circle";
                            }?>"></i></a>
                            </div>
                          </li>
                        <?php endforeach;?>
                        <div class="col-12 col-sm-6 col-xl-6 mt-3 mt-sm-0 text-center text-sm-start" style="margin: 10px 0;">
                          <a class="button button__primary d-inline-block w-auto address-container__button--add-adress border-custom-1" data-bs-toggle="offcanvas" href="#addAddressOffCanvas" role="button" aria-controls="offcanvasExample" style="margin: 10px 0;">Add Address<span class="ms-1"><i class="bi bi-plus"></i></span
                          ></a>
                        </div>
                      </ul>
                    <div class="payment-methods-container mt-5">
                      <div class="d-flex flex-wrap justify-content-center justify-content-sm-between align-items-center">
                        <?php echo form_open_multipart('paypal/buyProduct', array('class'=>'form login-form', 'autocomplete'=>"off"));?>
                          <?php foreach ($product_details as $key => $product_details_data):?>
                          <?php echo form_input(['name'=>'product_id[]','type' => 'hidden', 'value' => $product_details_data['product_id']]);?>
                          <?php echo form_input(['name'=>'product_qty[]','type' => 'hidden', 'value' => $product_details_data['product_qty']]);?>
                          <?php endforeach;?>
                          <?php if (!empty($data['user_address'])):?>
                            <button type="submit" class="button button-primary d-block d-md-inline-block d-lg-block text-center">Place Order</button>
                          <?php endif;?>
                          
                        <?php echo form_close();?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


          <?php if (count($data['cart_product'])>0):?>
              <div class="col-12 col-md-12 col-lg-4 col-xxl-3 mt-5 mt-lg-0" id="checkout_section">
                  <div class="cart-total-wrapper bg--light border-custom-1 border-radius--custom p-4">
                  <div class="cart__subtotal-container d-flex justify-content-between align-items-center mb-3">
                      <p class="text--sm text--primary m-0">Subtotal</p>
                      <h5 class="text--secondary text--lg m-0">
                      <?='$'.$totalamount.'.00'?>
                      </h5>
                  </div>
                  <div class="cart__subtotal-container cart__subtotal-container--shipping d-flex justify-content-between align-items-center mb-3">
                      <p class="text--sm text--primary m-0">Flat Rate</p>
                      <h5 class="text--secondary text--lg m-0"><span class="shipping-charge__prefix text--sm text--primary me-1"></span><span class="shipping-charge__value"><?php $flat_rate = '5.00';echo '$'.$flat_rate;?></span></h5>
                  </div>
                  <div class="cart__total-container d-flex justify-content-between align-items-center mb-3">
                      <p class="text--lg text--primary m-0">Total</p>
                      <h5 class="text--secondary m-0"><?php  $final_amount = $totalamount+$flat_rate; echo "$".$final_amount.".00";?></h5>
                  </div>
                  <div class="text-center">
                      <?php //echo "<pre>"; print_r($product_details);?>
                      <button id="address_section_trigger_btn" class="button button-primary d-block d-md-inline-block d-lg-block text-center w-100">Checkout</button>
                  </div>
                  </div>
              </div>
          <?php endif;?>
      </div>
    </div>
  </section>
  <!-- add address offcanvas -->
  <div class="offcanvas offcanvas-start" tabindex="-1" id="addAddressOffCanvas" aria-labelledby="offcanvasLabel">
    <div class="offcanvas-header justify-content-lg-end">
      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <?php echo form_open_multipart(base_url('user/add_address_init'), array('class'=>'add-address-form form','id'=>'addAddressForm'));?>
          <div class="row">
              <div class="form__field col-12">
                <label for="Add Address">Address<span class="text--darkRed">*</span></label>
                <input type="text" class="add-address-form__input-address" name="address" required/>
              </div>
              <div class="form__field col-12">
                <label for="Add Address">Landmark<span class="text--darkRed">*</span></label>
                <input type="text" class="add-address-form__input-door-flat-no" name="landmark" required/>
              </div>
              <div class="form__field col-6">
                <label for="Add Address">City<span class="text--darkRed">*</span></label>
                <input type="text" class="add-address-form__input-city" name="city" required/>
              </div>
              <div class="form__field col-6">
                <label for="Add Address">Pin Code<span class="text--darkRed">*</span></label>
                <input type="text" class="add-address-form__input-pin" name="zip_code" required/>
              </div>
              <div class="col-12 mt-2">
                <button class="d-block button button-primary w-100">Save address &amp; proceed</button>
              </div>
          </div>
        <?php echo form_close();?>
    </div>
  </div>

</main>
<?php $this->load->view('template/footer')?>

<script>
  function deletecartproduct(id){
    var message = "Are you sure you want to delete this product from the cart";
    var url = 'user/deletecartproduct/'+id;
    confirm(message);
    window.location.href = global_config.base_url+url;
  }

  function deleteAllCartProduct(user_id){
    var message = "Are you sure you want to delete this product from the cart";
    var url = 'user/deleteAllCartProduct/'+user_id;
    confirm(message);
    window.location.href = global_config.base_url+url;
  }

  $(document).ready(function(){
    $('#address_section_trigger_btn').on('click', function(){
      $('#address_section').css('display','block');
      $('#cart_table').css('display','none');
      $(this).parent().hide();
    });

    $('#place_order').on('click', function(){

      var product_details = '<?php echo json_encode($product_details)?>';
      var user_id = "<?php echo $this->session->userdata('user_id');?>";
      var address = $('li#address_id').attr('data_address');
      var total_amount  = "<?php echo $totalamount;?>";
      var url = "paypal/buyProduct";
    
      $.ajax({
        url: global_config.base_url+url,
        type: 'POST',
        data: {product_details: product_details, user_id: user_id, total_amount: total_amount, address: address},
        error: function() {
          //notificationToastUpdate('error', "The product Has been added to Wishlist successfully!");
        },
        success: function(data) {
          console.log("test");
          //notificationToastUpdate('success', "The product Has been added to Wishlist successfully!");
          //location.reload();
        }
      });
    });

  });

</script>

<style>
  header{
    z-index:100 !important;
  }
</style>