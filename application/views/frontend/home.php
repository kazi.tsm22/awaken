    <?php $this->load->view('template/header');?>
    
    <main class="site-content">
      <!-- Hero Section -->
      <section class="hero-section position-relative">
        <div class="container">
          <div class="row align-items-center pt-5 pt-lg-0">
            <div class="col-12 col-lg-6 pt-4 pt-lg-0 text-center text-lg-start mb-4 mb-lg-0 pe-lg-4 pe-xl-5">
              <h1 class="text--heading fw--600 mb-3 pt-5 pt-lg-0">Hydroelectric Turbine Control Equipment</h1>
              <p class="text--para mb-4">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus officiis praesentium ad, rem illo eos.</p>
              <a href="<?php echo base_url('home/products');?>" class="d-inline-block button button-primary">Shop Now</a>
            </div>
            <div class="col-12 col-lg-6">
              <div class="swiper swiper-hero">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                  <!-- Slides -->
                  <div class="swiper-slide d-flex align-items-center justify-content-center">
                    <img src="./assets/images/banner-1.jpg" alt="banner-1" />
                  </div>
                  <div class="swiper-slide d-flex align-items-center justify-content-center">
                    <img src="./assets/images/banner-2.jpg" alt="banner-2" />
                  </div>
                  <div class="swiper-slide d-flex align-items-center justify-content-center">
                    <img src="./assets/images/banner-3.jpg" alt="banner-3" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="shape-1">
          <img src="./assets/images/hero-section-polygon-2.svg" alt="" />
        </div>
        <div class="shape-2">
          <img src="./assets/images/hero-section-polygon-2.svg" alt="" />
        </div>
      </section>
      <!-- About Section -->
      <section class="about-section section--padding">
        <div class="container">
          <div class="row text-center text-lg-start">
            <div class="col-12 col-lg-3">
              <p class="text--secondary section__subheading mb-2">Welcome</p>
              <h2 class="section__heading text--heading mb-3">About Us</h2>
            </div>
            <div class="col-12 col-lg-9 about-section__content">
              <h3 class="text--heading mb-4 text-center text-lg-start">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste officiis ipsum provident quaerat nesciunt dolore!</h3>
              <div class="row">
                <div class="col-12 col-lg-6 pe-3">
                  <p class="text--para">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas in hic nisi, placeat rerum provident quasi consequuntur odio vel error dignissimos fuga magni, deserunt, excepturi maxime dolore molestias ipsa amet. Sed repudiandae voluptatum, ex blanditiis ducimus voluptas. Consequatur, sapiente velit.</p>
                </div>
                <div class="col-12 col-lg-6 ps-3">
                  <p class="text--para">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laboriosam quo asperiores ab omnis ipsum sequi numquam sunt iste, odio minus.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Why us section -->
      <section class="section--padding why-us-section position-relative bg--primary">
        <div class="container">
          <div class="row align-items-center flex-column-reverse flex-lg-row">
            <div class="col-12 col-md-12 col-lg-7">
              <div class="row">
                <div class="col-6 mb-4">
                  <div class="why-us-block bg--white border-radius--custom box-shadow--custom">
                    <img src="./assets/images/easy-installation.png" alt="Easy Installation" class="why-us-block__icon mb-3" />
                    <h4 class="why-us-block__title text--heading">Easy Installation</h4>
                    <p class="text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem, doloribus!</p>
                  </div>
                </div>
                <div class="col-6 mb-4">
                  <div class="why-us-block bg--white border-radius--custom box-shadow--custom">
                    <img src="./assets/images/high-technology.png" alt="Advanced Technology" class="why-us-block__icon mb-3" />
                    <h4 class="why-us-block__title text--heading">Advanced Technology</h4>
                    <p class="text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem, doloribus!</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="why-us-block bg--white border-radius--custom box-shadow--custom">
                    <img src="./assets/images/cost-solution.png" alt="Cost Solution" class="why-us-block__icon mb-3" />
                    <h4 class="why-us-block__title text--heading">Cost Solution</h4>
                    <p class="text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem, doloribus!</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="why-us-block bg--white border-radius--custom box-shadow--custom">
                    <img src="./assets/images/24x7-support.png" alt="24x7 Support" class="why-us-block__icon mb-3" />
                    <h4 class="why-us-block__title text--heading">24x7 Support</h4>
                    <p class="text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem, doloribus!</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-5 mb-5 mb-lg-0 text-center text-lg-start ps-lg-4 ps-xl-5">
              <p class="text--light-muted section__subheading mb-2">Why Us</p>
              <h2 class="text--light section__heading">Lorem ipsum dolor sit amet consectetur.</h2>
              <p class="text--light-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt delectus id similique facere neque corporis.</p>
            </div>
          </div>
        </div>
      </section>
      <!-- Featured Products Section -->
      <section class="featured-products-section section--padding">
        <div class="container">
          <h2 class="section__heading text--heading text-center mb-5">Featured Products</h2>
          <div class="swiper swiper-featured-products">

            
              <div class="swiper-wrapper">
                <?php foreach($data['products'] as $products):?>
                  <div class="swiper-slide d-flex align-items-center justify-content-center">
                    <div class="product-card border-radius--custom h-100">
                      <a href="<?php echo base_url('home/product_details/'.$products['id']);?>" class="product-card__thumbnail-container text-center p-3 d-block" style="height:240px">
                        <img style="height:240px" src="<?php print_r(base_url('assets/images/products/'.json_decode($products['img'], TRUE)['images_0']));?>" alt="ATE-37 Power Amplifier" class="d-inline-block" />
                      </a>
                      <div class="product-card__body p-3">
                        <div class="product-card__title text--heading mt-4 text-center">
                          <a href="<?php echo base_url('home/product_details/'.$products['id']);?>"><?php echo $products['name']?></a>
                        </div>
                        <div class="product-card__price">
                          <p class="text--secondary text-center"><?php echo "$".$products['selling_price'].".00"?> <span class="ms-3 text--para text-decoration-line-through"><?php echo "$".$products['mrp'].".00"?></span> </p>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach;?>
              </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
          </div>
        </div>
      </section>
      <section class="testimonial-section section--padding bg--light">
        <div class="container">
          <p class="text--secondary section__subheading text-center mb-2">Testimonial</p>
          <h2 class="text--heading section__heading text-center mb-5">What client say about us</h2>
          <div class="swiper swiper-testimonial pb-5">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper text-center">
              <!-- Slides -->
              <div class="swiper-slide">
                <div class="testimonial">
                  <div class="testimonial__author-img-container text-center">
                    <img src="./assets/images/testimonials/c-1.jpg" alt="Author Title" />
                  </div>
                  <div class="testimonial-body bg--white border-radius--custom box-shadow--custom">
                    <p class="author__quote text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt officia blanditiis dolor debitis. Eveniet, pariatur?</p>
                    <div class="author__content">
                      <p class="author__title text--heading fw--semibold mt-0 mb-1">John Doe</p>
                      <p class="text--sm text--primary m-0">CEO, ABC Corp.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial">
                  <div class="testimonial__author-img-container text-center">
                    <img src="./assets/images/testimonials/c-2.jpg" alt="Author Title" />
                  </div>
                  <div class="testimonial-body bg--white border-radius--custom box-shadow--custom">
                    <p class="author__quote text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt officia blanditiis dolor debitis. Eveniet, pariatur?</p>
                    <div class="author__content">
                      <p class="author__title text--heading fw--semibold mt-0 mb-1">John Doe</p>
                      <p class="text--sm text--primary m-0">CEO, ABC Corp.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial">
                  <div class="testimonial__author-img-container text-center">
                    <img src="./assets/images/testimonials/c-3.jpg" alt="Author Title" />
                  </div>
                  <div class="testimonial-body bg--white border-radius--custom box-shadow--custom">
                    <p class="author__quote text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt officia blanditiis dolor debitis. Eveniet, pariatur?</p>
                    <div class="author__content">
                      <p class="author__title text--heading fw--semibold mt-0 mb-1">John Doe</p>
                      <p class="text--sm text--primary m-0">CEO, ABC Corp.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial">
                  <div class="testimonial__author-img-container text-center">
                    <img src="./assets/images/testimonials/c-4.jpg" alt="Author Title" />
                  </div>
                  <div class="testimonial-body bg--white border-radius--custom box-shadow--custom">
                    <p class="author__quote text--para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt officia blanditiis dolor debitis. Eveniet, pariatur?</p>
                    <div class="author__content">
                      <p class="author__title text--heading fw--semibold mt-0 mb-1">John Doe</p>
                      <p class="text--sm text--primary m-0">CEO, ABC Corp.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>

            <!-- If we need navigation buttons
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>-->
          </div>
        </div>
      </section>
    </main>

    <?php $this->load->view('template/footer');?>