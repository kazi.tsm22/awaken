<?php include('templete/header.php');?>
<?php include('templete/side_panel.php');?>

<?php
    // $CI =& get_instance();
    // $CI->load->model('Admin_model');
    // $product_name = $CI->Admin_model->buy_product();
?>
    <main class="main-content bgc-grey-100">
        <div id="mainContent">
            <div class="row gap-20 masonry pos-r">
                <div class="masonry-sizer col-md-6"></div>
                <div class="masonry-item col-md-12">
                    <div class="bgc-white p-20 bd">
                        <h6 class="c-grey-900">
                           Add Product
                        </h6>

                        <?php if (form_error('name') || form_error('mrp') || form_error('selling_price')) { ?>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 p-0">
                                        <div class="alert alert-danger" style="background-color: #ffffff;">
                                            <?php echo form_error('name'); ?>
                                            <?php echo form_error('mrp'); ?>
                                            <?php echo form_error('selling_price'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="mT-30">
                            <?php echo form_open_multipart('admin/add_product_init');?>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label >Name</label>
                                        <?php echo form_input(['name' => 'name', 'class' => 'form-control', 'value' => set_value('name')]) ?>
                                    </div>
                                    
                                    <div class="form-group col-md-4">
                                        <label>MRP Price</label>
                                        <?php echo form_input(['name' => 'mrp', 'class' => 'form-control', 'value' => set_value('mrp')]) ?>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label>Selling Price</label>
                                        <?php echo form_input(['name' => 'selling_price', 'class' => 'form-control', 'value' => set_value('selling_price')]) ?>
                                    </div>

                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="inputState">Image </label>
                                        <?php echo form_input(['name' => "userfile[]", "multiple"=>"multiple", 'type' => 'file', 'class' => 'form-control','required' => 'required']); ?>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Description</label>
                                        <textarea name="description" class="form-control" height="150"></textarea>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <br><br><br><br><br><br>
                                    <div class="form-group col-md-8 w-100 mt-5"></div>
                                    <div class="form-group col-md-2 w-100">
                                        <button type="submit" class="btn btn-outline-primary w-100">Upload</button>
                                    </div>
                                    <div class="form-group col-md-2 w-100">
                                        <a href="<?=base_url('admin/products')?>" class="btn btn-outline-danger w-100">Cancel</a>
                                    </div>
                                    
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </main>
<style>
    
/* css for the switcher */
html,
body {
  height: 100%;
  background-color: #D1D5DB;
}

.switch-wrapper {
  display: grid;
  place-content: center;
  min-height: 100%;
}

.switch {
  display: none;
}

.switch + div {
  width: 48px;
  height: 24px;
  border-radius: 12px;
  background-color: #ff1313;
  transition: background-color 200ms;
  cursor: pointer;
}

.switch:checked + div {
  background-color: #00a850;
}

.switch + div > div {
  width: 24px;
  height: 24px;
  border-radius: 23px;
  background-color: #fff;
  transition: transform 250ms;
  pointer-events: none;
}

.switch:checked + div > div {
  transform: translateX(28px);
}
</style>
<?php include('templete/footer.php');?>